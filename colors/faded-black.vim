" Vim color file
" Maintainer:           John Kaul
" Created On:           2003 Mar 19
" File Last Updated:    03.14.20 08:49:01
" Contributers:         See Contributers.txt file
"
" DESCRIPTION
" This is a dark theme with slightly faded colors.
"
" NOTES
" This colorscheme started as a faded version of of the 'Chocolate
" Liquor', but soon took on a life of its own. Easy on the eyes, but
" still has good contrast.  There isn't any 'Chocolate Liquor' left in
" this file at this point, but I thought I'd document it's roots.
"
"
" I hope you enjoy...
"
" LOG
" 01.07.05 update: Tweeked a few colors and added terminal colors.
" Please note that I didnt go thru the terminal colors very well. (I
" just added a few ''dif.  then normal'' colors) If you use the terminal
" alot I would make an attempt to fix anything you dont like about this
" color scheme if I were you. :)~  Other than that, I just called it it
" good 'nuff.
"
" 01.16.13 update: 
"    o  Changed a few more colors (still havent looked at the
"       terminal colors).
"
" 04.12.13 update:
"    o  Search background color
"    o  Include statement foreground color
"    o  Status lines colors
"
" 04.23.13 update: 
"    o  Added and adjusted some terminal colors.
"
" 03.12.20 update: 
"    o  Adjusted some GUI colors for fadding eyes.
"    o  Adjusted the TERM colors to match gui.

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "faded-black"


" Vim >= 7.0 specific colors
if version >= 700
  hi CursorLine   ctermbg=NONE        ctermfg=NONE        guibg=#2d2d2d       guifg=NONE      term=NONE           gui=NONE
  hi CursorColumn ctermbg=236         ctermfg=NONE        guibg=#2d2d2d       guifg=NONE      term=NONE           gui=NONE
  hi MatchParen   ctermbg=240         ctermfg=1           guibg=#2f2f2f       guifg=#8b3a3a   term=NONE           gui=bold
  hi Pmenu        ctermbg=green       ctermfg=255         guibg=#444444       guifg=#ffffff   term=NONE           gui=NONE
  hi PmenuSel     ctermbg=148         ctermfg=0           guibg=#b1d631       guifg=#000000   term=NONE           gui=NONE
  "_x_ hi PmenuSbar
  "_x_ hi PmenuThumb
  " //-- Spell --// {{{
  hi SpellBad     ctermbg=237         ctermfg=209         guibg=#1c2026       guifg=#bc6c4c   term=NONE           gui=NONE
  hi SpellCap     ctermbg=237         ctermfg=103         guibg=#6c6c9c       guifg=Black     term=NONE           gui=NONE
  hi SpellRare    ctermbg=237         ctermfg=139         guibg=#bc6c9c       guifg=Black     term=NONE           gui=bold
  hi SpellLocal   ctermbg=237         ctermfg=151         guibg=#7cac7c       guifg=Black     term=NONE           gui=NONE
  " }}}
endif

" //-- Stuff --//
hi Cursor       ctermbg=1           ctermfg=7           guibg=#8b3a3a       guifg=#b5b3b3                       gui=NONE
" hi Normal       ctermbg=NONE        ctermfg=LightGrey   guibg=#03080F       guifg=#958a73                       gui=NONE
hi Normal       ctermbg=NONE        ctermfg=252         guibg=#03080F       guifg=#dbd7cf                       gui=NONE
" The text color between the above two are tough to decide; I leave it up to you.
hi NonText      ctermbg=NONE        ctermfg=Black       guibg=#03080F       guifg=#000030                       gui=NONE
hi DiffDelete   ctermbg=DarkRed     ctermfg=White       guibg=DarkRed       guifg=White                         gui=NONE
hi DiffAdd      ctermbg=DarkGreen   ctermfg=White       guibg=DarkGreen     guifg=White                         gui=NONE
hi DiffText     ctermbg=LightCyan   ctermfg=Yellow      guibg=Lightblue     guifg=Yellow                        gui=NONE
hi DiffChange   ctermbg=LightBlue   ctermfg=White       guibg=LightBlue3    guifg=White                         gui=NONE
hi Constant     ctermbg=NONE        ctermfg=darkred     guibg=NONE          guifg=#872e30                       gui=NONE
" hi StatusLine   ctermbg=DarkGrey    ctermfg=Red         guibg=#2a2a2a       guifg=#eeeeee                       gui=italic
hi StatusLine   ctermbg=Black       ctermfg=1           guibg=#2a2a2a       guifg=LightRed                      gui=NONE
hi StatusLineNC ctermbg=Darkgrey    ctermfg=Black       guibg=#515151       guifg=Black                         gui=NONE
hi VertSplit 	ctermbg=238         ctermfg=238         guibg=#444444       guifg=#444444                       gui=none 
hi Visual	ctermbg=Darkgrey    ctermfg=252         guibg=#3c414c       guifg=#faf4c6                       gui=none  

hi Search       ctermbg=230         ctermfg=236         guibg=#596e10       guifg=#03080F                       gui=NONE
hi Label        ctermbg=NONE        ctermfg=NONE        guibg=NONE          guifg=#ffc0c0                       gui=NONE
hi LineNr       ctermbg=NONE        ctermfg=darkgrey    guibg=NONE          guifg=#5C4C47                       gui=NONE
"_x_ hi link Tag PreProc

" //-- Messages --//
hi Question     ctermbg=NONE        ctermfg=DarkGreen   guibg=NONE          guifg=SeaGreen term=standout       gui=bold
hi MoreMsg      ctermbg=NONE        ctermfg=DarkGreen   guibg=NONE          guifg=SeaGreen term=bold,italic    gui=bold
hi ErrorMsg     ctermbg=DarkGrey    ctermfg=Red         guibg=#2a2a2a       guifg=Black                        gui=none
"_x_ hi MoreMsg
"_x_ hi ModeMsg
hi link WarningMsg ErrorMsg
"_x_ hi Error
"_x_ hi Debug
"_x_ hi Ignore

" //-- Syntax group --//
hi Comment      ctermbg=NONE        ctermfg=238         guibg=NONE          guifg=#676b60                      gui=italic
hi PreProc      ctermbg=NONE        ctermfg=65          guibg=NONE          guifg=#728766                      gui=NONE
hi link Macro PreProc
hi link Define PreProc
hi link Typedef PreProc
"_x_ hi PreCondit
"_x_ hi Constant
" hi Character    ctermbg=NONE        ctermfg=222         guibg=NONE          guifg=#a58052                      gui=NONE
hi link Character LineNr
hi link Boolean PreProc
hi Statement 	ctermbg=NONE        ctermfg=24          guibg=NONE          guifg=#136187                      gui=NONE 
hi Type         ctermbg=NONE        ctermfg=222         guibg=NONE          guifg=#a58052                      gui=NONE
hi Identifier   ctermbg=NONE        ctermfg=23          guibg=NONE          guifg=#737d95                      gui=NONE
hi Special      ctermbg=NONE        ctermfg=7           guibg=NONE          guifg=#5b5646                      gui=NONE
"_x_ hi SpecialKey   ctermbg=NONE        ctermfg=7           guibg=NONE          guifg=#5b5646                      gui=NONE
hi Todo         ctermbg=NONE        ctermfg=darkmagenta guibg=NONE          guifg=LightBlue                    gui=bold,italic
hi Number       ctermbg=NONE        ctermfg=darkcyan    guibg=NONE          guifg=LightBlue                    gui=NONE
hi link Float Number
"_x_ hi StorageClass
"_x_ hi Structure
"_x_ hi Exception

" //-- Lisp stuff --//
hi lispAtomMark  ctermbg=NONE       ctermfg=cyan        guifg=darkcyan      guibg=NONE                         gui=NONE
hi lispNumber    ctermbg=NONE       ctermfg=DarkGreen   guifg=lightblue     guibg=NONE                         gui=NONE
hi link lispVar Spcial
hi link lispFunc Statement
hi link lispEscapeSpecial Boolean

" //-- Fold --//
hi Folded       ctermbg=NONE        ctermfg=darkgrey    guibg=#001a33       guifg=#7c6f6b                      gui=italic
hi FoldColumn   ctermbg=NONE        ctermfg=Yellow      guibg=#6699CC       guifg=#0000EE                      gui=NONE

hi Underlined	cterm=underline     ctermfg=5
hi Directory	                    ctermfg=24                              guifg=#136187
hi Title	                    ctermfg=5
hi WildMenu	ctermbg=3           ctermfg=0 
hi ModeMsg	cterm=NONE          ctermfg=brown

" //-- Diff --//{{{
"_x_ hi DiffDelete
"_x_ hi DiffAdd
"_x_ hi DiffChange
"_x_ hi DiffText
"}}}
" //-- Markdown --// {{{
hi link markdownHeadingRule PreProc
hi link markdownHeadingDelimiter Statement
hi link markdownH1 PreProc
hi link markdownH2 Statement
hi link markdownH3 Type
hi link markdownH4 Constant
hi link markdownH5 LineNr
hi link markdownH6 Number
hi link markdownListMarker Constant
"_x_ hi markdownOrderedListMarker
"_x_ hi markdownItalic
"_x_ hi markdownBold
hi link markdownLinkText Statement
hi link markdownLinkDelimiter Type
hi link markdownLinkTextDelimiter markdownLinkDelimiter
"_x_ hi markdownIdDeclaration
"_x_ hi markdownAutomaticLink
hi link markdownUrl markdownLinkText
"_x_ hi markdownUrldelimiter
hi link markdownCodeDelimiter Special
hi link markdownCode Comment
hi link markdownCodeBlock markdownCode
" }}}
" //-- Vim --// {{{
"_x_ hi VimCommentTitle
"_x_ hi VimMapMod
"_x_ hi VimMapModKey
"_x_ hi VimNotation
"_x_ hi VimBracket
" }}}
