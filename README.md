# README #
This is a dark color scheme for Vim. Easy on the eyes with slightly faded colors. Matching terminal colors.

### How do I get set up? ###
Post Vim 8.0 we now have the ability to load `PACKs`. This plugin should be loaded into the `opt` directory.

    cd ~/.vim/pack/*/opt/
    git clone https://JohnKaul@bitbucket.org/JohnKaul/faded-black.vim.git

In your .vimrc add the following:

    packadd faded-black

However, if you don't have a newer verion of Vim installed, I recommend installing [pathogen.vim](https://github.com/tpope/vim-pathogen), and then simply copy and paste:

    cd ~/.vim/bundle
    git clone https://JohnKaul@bitbucket.org/JohnKaul/faded-black.vim.git

Otherwise you can always download this repository and place the `faded-black.vim` file in the `colors` directory. 

### Git Standards ###
Each commit will be structured like this:
`"main.cpp: Formatting cleanup."`
Where the file name is listed first followed by a colon and a brief description of the change.

### Coding Standards ###
1. Each file should contain the line: `// File Last Updated: ` followed by the date. I use a simple vim mapping for this.

### Who do I talk to? ###

* John Kaul - john.kaul@outlook.com
